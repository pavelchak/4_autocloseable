package com.pavelchak;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        int foo = 0;
        try (Apple apple = new Apple("Gold")) {
            System.out.println("Now I am doing pause....");
            Thread.sleep(1000);
            if (foo == 0) {
                throw new IOException("exception in block 'try with resource'");
            }
            System.out.println("Hello, my name is " + apple.getName());
        }
        catch (IOException e){
            System.out.println("Do not worry, everything is OK");
            System.out.println("This is " + e.getMessage());
            Thread.sleep(5000);
        }

        for (int i = 0; i < 5; i++) {
            try (Apple apple = new Apple()) {
                System.out.println("Now I am doing pause....");
                Thread.sleep(1000);
                System.out.println("Hello, my name is " + apple.getName());
            }
        }
    }
}
