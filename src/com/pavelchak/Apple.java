package com.pavelchak;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Apple implements AutoCloseable {
    private String name;
    private LocalDateTime dateCreate;
    private LocalDateTime dateClose;
    private PrintWriter file;
    private int numberObj;
    private static int counterApple = 0;

    public Apple(String name) {
        this.name = name;
        counterApple++;
        numberObj = counterApple;
        this.dateCreate= LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        System.out.println();
        System.out.println("Object name: " + this.name);
        System.out.println("Object number: " + this.numberObj);
        System.out.println("Creation time: " + this.dateCreate.format(formatter));
        try {
            file = new PrintWriter(this.name + ".txt");
            this.dateCreate = LocalDateTime.now();
            file.println();
            file.println("Object name: " + this.name);
            file.println("Object number: " + this.numberObj);
            file.println("Creation time: " + this.dateCreate.format(formatter));
        } catch (FileNotFoundException e) {
            System.out.println("File not created");
        }
    }

    public Apple() {
        counterApple++;
        this.name = "NoName"+counterApple;
        numberObj = counterApple;
        this.dateCreate = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        System.out.println();
        System.out.println("Object name: " + this.name);
        System.out.println("Object number: " + this.numberObj);
        System.out.println("Creation time: " + this.dateCreate.format(formatter));
        try {
            file = new PrintWriter(this.name + ".txt");
            file.println();
            file.println("Object name: " + this.name);
            file.println("Object number: " + this.numberObj);
            file.println("Creation time: " + this.dateCreate.format(formatter));
        } catch (FileNotFoundException e) {
            System.out.println("File not created");
        }
    }

    public void close() throws IOException {
        if(counterApple==5){
            throw new IOException("Exception: method 'close()'");
        }
        this.dateClose = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        System.out.println("------------------");
        System.out.println("Close object " + this.name);
        System.out.println("Closing time: " + this.dateClose.format(formatter));

        if(file!=null){
            file.println();
            file.println("Closing time: " + this.dateClose.format(formatter));
            file.close();
        }
    }

    public String getName() {
        return name;
    }

    public static int getCounterApple() {
        return counterApple;
    }
}
